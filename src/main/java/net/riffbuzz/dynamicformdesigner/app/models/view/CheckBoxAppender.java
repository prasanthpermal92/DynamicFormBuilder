package net.riffbuzz.dynamicformdesigner.app.models.view;

import android.app.Activity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.riffbuzz.dynamicformdesigner.app.models.models.Elements;


public class CheckBoxAppender {
    public static View getCheckBox(Activity context,Elements elements)
    {
        String textSize = elements.getText_size();
        String hint = elements.getLabel();
        String[] values = elements.getValue();

        String style = elements.getStyle();
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        linearLayout.setLayoutParams(params);
        TextView textview = new TextView(context);
        textview.setText(hint);
        textview.setTextSize(Float.valueOf(textSize));
        linearLayout.addView(textview);
        for (String value:values
             ) {
            CheckBox checkBox = new CheckBox(context);
            checkBox.setText(value);
            linearLayout.addView(checkBox);
        }

        return linearLayout;
    }
}
