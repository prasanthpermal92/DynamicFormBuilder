package net.riffbuzz.dynamicformdesigner.app.models.models;

import java.util.ArrayList;

/**
 * Created by Prasanth on 21/10/15.
 */
public class Form {
    String status;
    ArrayList<Elements> elements;
    String form_title;

    public String getForm_title() {
        return form_title;
    }

    public void setForm_title(String form_title) {
        this.form_title = form_title;
    }

    public ArrayList<Elements> getElements() {
        return elements;
    }

    public void setElements(ArrayList<Elements> elements) {
        this.elements = elements;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
