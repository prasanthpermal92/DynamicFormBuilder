package net.riffbuzz.dynamicformdesigner.app.models.view;

import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import net.riffbuzz.dynamicformdesigner.app.models.models.Elements;


public class RadioGroupAppender {
    public static View getRadioGroup(Activity context,Elements elements)
    {
        String textSize = elements.getText_size();
        String hint = elements.getLabel();
        String[] values = elements.getValue();
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        linearLayout.setLayoutParams(params);
        TextView textview = new TextView(context);
        textview.setText(hint);
        textview.setTextSize(Float.valueOf(textSize));
        linearLayout.addView(textview);
        RadioGroup radioGroup = new RadioGroup(context);
        for (String value:values
             ) {
            RadioButton radioButton = new RadioButton(context);
            radioButton.setText(value);
            radioGroup.addView(radioButton);
        }
        linearLayout.addView(radioGroup);
        return linearLayout;
    }
}
