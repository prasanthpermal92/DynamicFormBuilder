package net.riffbuzz.dynamicformdesigner.app.models.view;

import android.app.Activity;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import net.riffbuzz.dynamicformdesigner.app.models.models.Elements;


public class EditTextAppender  {
    public static View getEditText(Activity context,Elements elements)
    {
        String textSize = elements.getText_size();
        String hint = elements.getLabel();
        String[] value = elements.getValue();
        String style = elements.getStyle();
        EditText editText = new EditText(context);
        editText.setTextSize(Float.valueOf(textSize));
        if(hint!=null&&!hint.equals(""))
        editText.setHint(hint);
        if(value!=null&&value[0]!=null&&!value[0].equals(""))
        {
            editText.setText(value[0]);
            editText.setSelection(value[0].length());
        }
        editText.setBottom(10);
        if(style!=null&&style.contains("singleline"))
        {
            editText.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
            editText.setSingleLine(true);
            if(style.contains("name"))
            {
                editText.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
            }
            if (style.contains("password"))
            {
                editText.setInputType(EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);
                editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
            if (style.contains("number")){
                editText.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
            }
            if (style.contains("limit"))
            {
                int startposition = style.indexOf("limit_")+6;
                int endposition  = style.indexOf("pts");
                if(startposition>0&&endposition>0)
                {
                    String length_string = style.substring(startposition,endposition);
                    int length = Integer.valueOf(length_string);
                    Log.e("foot",length_string);
                    editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(length)});
                }

            }

        }
        else
        {
            editText.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
         //   editText.setInputType();
        }
        return editText;
    }
}
