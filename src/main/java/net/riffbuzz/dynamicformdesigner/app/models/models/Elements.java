package net.riffbuzz.dynamicformdesigner.app.models.models;

/**
 * Created by Prasanth on 21/10/15.
 */
public class Elements {
    String label;
    String type;
    String text_size;
    String style;
    String[] value;

    public String[] getValue() {
        return value;
    }

    public void setValue(String[] value) {
        this.value = value;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText_size() {
        return text_size;
    }

    public void setText_size(String text_size) {
        this.text_size = text_size;
    }

}
