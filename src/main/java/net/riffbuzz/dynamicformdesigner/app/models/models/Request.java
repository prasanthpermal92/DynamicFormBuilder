package net.riffbuzz.dynamicformdesigner.app.models.models;

/**
 * Created by Prasanth on 21/10/15.
 */
public class Request {
    String field;
    String value;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
