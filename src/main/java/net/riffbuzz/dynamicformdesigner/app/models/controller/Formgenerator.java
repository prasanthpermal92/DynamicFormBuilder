package net.riffbuzz.dynamicformdesigner.app.models.controller;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.google.gson.Gson;

import net.riffbuzz.dynamicformdesigner.app.models.models.Elements;
import net.riffbuzz.dynamicformdesigner.app.models.models.Form;
import net.riffbuzz.dynamicformdesigner.app.models.view.AutoCompleteAppender;
import net.riffbuzz.dynamicformdesigner.app.models.view.CheckBoxAppender;
import net.riffbuzz.dynamicformdesigner.app.models.view.EditTextAppender;
import net.riffbuzz.dynamicformdesigner.app.models.view.RadioGroupAppender;

import java.util.ArrayList;

/**
 * Created by Prasanth on 21/10/15.
 */
public class Formgenerator {
    public static void generateform(AppCompatActivity context, LinearLayout parent, String json) {
        Form myform = new Gson().fromJson(json, Form.class);
        if (myform != null && myform.getStatus().equals("success")) {
            context.getSupportActionBar().setTitle(myform.getForm_title());
            ArrayList<Elements> allelements = myform.getElements();
            for (Elements element : allelements) {
                String viewType = element.getType();
                View view;
                if (viewType.equals("textfield")) {
                    view = EditTextAppender.getEditText(context,element);
                    parent.addView(view);
                }
                else if(viewType.equals("dropdown"))
                {
                    view = AutoCompleteAppender.getAutoComplete(context,element);
                    parent.addView(view);
                }
                else if(viewType.equals("checkbox"))
                {
                    view = CheckBoxAppender.getCheckBox(context, element);
                    parent.addView(view);
                }
                else if(viewType.equals("radio"))
                {
                    view = RadioGroupAppender.getRadioGroup(context, element);
                    parent.addView(view);
                }

            }
        }
    }
}
