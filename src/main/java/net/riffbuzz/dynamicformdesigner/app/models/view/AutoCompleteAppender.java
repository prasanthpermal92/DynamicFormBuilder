package net.riffbuzz.dynamicformdesigner.app.models.view;

import android.app.Activity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;

import net.riffbuzz.dynamicformdesigner.app.models.models.Elements;


public class AutoCompleteAppender {
    public static View getAutoComplete(Activity context,Elements elements)
    {
        String textSize = elements.getText_size();
        String hint = elements.getLabel();
        String[] value = elements.getValue();
        String style = elements.getStyle();
        final AutoCompleteTextView autoCompleteTextView = new AutoCompleteTextView(context);
        autoCompleteTextView.setBottom(10);
//        autoCompleteTextView.setTop(30);
//        autoCompleteTextView.setPadding(25,0,0,10);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
                ((int) LinearLayout.LayoutParams.MATCH_PARENT, (int) LinearLayout.LayoutParams.WRAP_CONTENT);
        autoCompleteTextView.setLayoutParams(params);
        autoCompleteTextView.setEms(10);
        autoCompleteTextView.setThreshold(1);
        autoCompleteTextView.setTextSize(Float.valueOf(textSize));
        if(hint!=null&&!hint.equals(""))
            autoCompleteTextView.setHint(hint);
        if(value!=null&&value.length>0)
        {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>
                    (context,android.R.layout.select_dialog_item,value);
            autoCompleteTextView.setAdapter(adapter);
        }
        autoCompleteTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                autoCompleteTextView.showDropDown();
            }
        });
        autoCompleteTextView.setKeyListener(null);
        autoCompleteTextView.setFocusable(false);
        autoCompleteTextView.setFocusableInTouchMode(false);
        return autoCompleteTextView;
    }
}
